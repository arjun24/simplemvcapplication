﻿$(document).ready(function () {
    $('#drpcity').val($.urlParam('city'));
    //var cit = $.urlParam('city');
    $('#drpcity').change(function () {
        window.location.href = "http://localhost:52196/account/search?city=" + $('#drpcity').val();
    });
});
$.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results == null) {
        return null;
    }
    else {
        return decodeURIComponent(results[1]) || 0;
    }
}