﻿$(document).ready(function () {
    formvalidation();
 });
 var formvalidation = function () {
        $('#frmregister').validate({
            rules: {
                FirstName: {
                    required: true
                },
                LastName: {
                    required: true
                },
                Email: {
                    required: true,
                    email: true
                },
                Age: {
                    required: true,
                    min: 10,
                   // number: true,
                    max:60
                }
            },
            messages: {
                FirstName: {
                    required : "First Name is required"
                },
                LastName: {
                    required: "Last Name is required"
                },
                Email: {
                    required: "Email address is required",
                    email : "Please enter valid email address"
                },
                Age: {
                    required: "Age is required",
                    min: "Age must be greater than 9",
                    max:"Age must be less than 61"
                }
            },
            submitHandler: function (form)
            {
                $.ajax({
                    type: "post",
                    url: "/Account/SaveRegister",
                    data: $('#frmregister').serialize(),
                    success: function (d) {

                    }
                });
            }
        });
    }
