﻿using MVCApplication.App;
using MVCApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MVCApplication.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SaveRegister(AccountModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    new Account().SaveResgister(model.FirstName, model.LastName, model.Email, model.Age);
                    FormsAuthentication.SetAuthCookie(model.FirstName, true);
                    //if (User.Identity.IsAuthenticated)
                    //{
                    //    string Name = HttpContext.User.Identity.Name;
                    //}
                }
            }
            catch { }
            return View("Register");
            //return RedirectToAction("Index", "Home");
        }
        [Authorize(Users = "arjun")]
        public ActionResult Search()
        {
            //string Name = HttpContext.User.Identity.Name;
            List<AccountModel> _lstaccount = new List<AccountModel>();
            for(int i =0;i<9;i++)
            {
                AccountModel _accountmodel = new AccountModel
                {
                    //test2 chk in
                    Age = 1,
                    Email = "ar@gmail.com",
                    FirstName = HttpContext.User.Identity.Name,
                    LastName = "Jadhav"
                };
                _lstaccount.Add(_accountmodel);
            }
            return View(_lstaccount);
        }
    }
}