﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCApplication.Models
{
    public class AccountModel
    {
        [Required]
        public string FirstName { get; set; }
        [Required(ErrorMessage ="Last name is mandatory")]
        public string LastName { get; set; }
        [Required(ErrorMessage =" Email is required")]

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        [Range(10,60)]
        public int Age { get; set; }
    }
}